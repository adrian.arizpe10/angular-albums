import {Injectable, Output, EventEmitter} from '@angular/core';
import {Observable, of} from "rxjs";
import {Album} from "../components/albums/albums.component";
import {HttpHeaders, HttpClient} from "@angular/common/http";

// const httpOptions = {
//   headers: new HttpHeaders({'Content-Type': 'Application'})
// };

@Injectable({
  providedIn: 'root'
})
export class AlbumsService {

  albums: Album[];
  data: Observable<any>;
//albumsLink: string = '';

  constructor() {
    this.albums = [
      {
        title: 'Toxicity',
        artist: 'System of a Down',
        songs: ['Chop Suey!', ' Toxicity', ' Aerials'],
        favorite: 'Chop Suey!',
        year:"09/04/2001",
        genre: 'Alternative metal',
        units: 12000000,
        cover: "/assets/images/SystemofaDownToxicityalbumcover.jpg"
      },
      {
        title: 'Nevermind',
        artist: 'Nirvanna',
        songs: ['Come As You Are', ' In Bloom', ' Drain You'],
        favorite: 'Come As You Are',
        year:"09/24/1991",
        genre: 'Punk Rock',
        units: 30000000,
        cover: '/assets/images/NirvanaNevermindalbumcover.jpg'
      },
      {
        title: 'Circles',
        artist: 'Mac Miller',
        songs: ['Complicated', ' Blue World', ' Good News'],
        favorite: 'Complicated',
        year:"01/17/2020",
        genre: 'Emo Rap',
        units: 164000,
        cover: '/assets/images/Mac_Miller_-_Circles.png'
      },
      {
        title: 'Three Cheers For Sweet Revenge',
        artist: 'My Chemical Romance',
        songs: ['Helena', ' Cemetery Drive', ' The Jetset Life Is Gonna Kill You'],
        favorite: 'Helena',
        year:"06/08/2004",
        genre: 'Emo Rock',
        units: 1356000,
        cover: '/assets/images/My-Chemical-Romance-Three-Cheers-LP-Vinyl-2076845_a595be04-a405-4bf2-9e15-79076c39c542_1024x1024.jpeg'
      },
      {
        title: 'Hank Williams JR Greatest Hits',
        artist: 'Hank Williams JR ',
        songs: ['Family Tradition', ' Whiskey Bent and Hell Bound', ' All My Rowdy Friends (Have Settled Down)'],
        favorite: 'Whiskey Bent and Hell Bound',
        year:"09/01/1982",
        genre: 'Country',
        units: 3410700,
        cover: '/assets/images/81Wp9hU81sL._SL1425_.jpg'
      }
    ];//End of array
  }//End of Constructor

  getAlbums(): Observable<Album[]> {
    //return this.http.get<Album[]>(this.albumsLink)
    return of(this.albums);
  }
  // saveAlbum(album: Album) : Observable<Album>{
  //   return this.http.post<Album>(this.albumsLink, album, httpOptions);
  // }
  addAlbum(album:Album ){
    this.albums.unshift(album);
  }

}
