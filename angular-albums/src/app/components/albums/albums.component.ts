import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AlbumsService} from "../../services/albums.service";

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.css']
})
export class AlbumsComponent implements OnInit {
  @Output() newAlbum: EventEmitter<Album> = new EventEmitter();
  album: Album = {
    artist: "", cover: "", favorite: "", genre: "",
    songs: [], title: "", units: 0, year: "0"
  };
  loadingAlbums: boolean = false;
  albums: Album[];
  showAlbums: boolean = false;
  showAlbumForm: boolean = false;
  data: any;

  constructor(private albumsService: AlbumsService) {}

  ngOnInit(): void {
    setTimeout(() => {this.loadingAlbums = true}, 5000)
    this.albumsService.getAlbums().subscribe(albums =>
    {
      this.albums = albums;
    })
    // this.albumsService.addAlbum()
  }//End of ngOnInit
  onSubmit(e) {
    console.log('submit works');
    e.preventDefault();
  }
  // addAlbum(title, artist){
  //   if (!title || !artist) {
  //     alert('Enter an Album\'s information');
  //   } else {
  //     this.albumsService.addAlbum({title, artist} as Album).subscribe(
  //       album => {
  //         this.newAlbum.emit(album);
  //       }
  //     )
  //   }
  // }
  // updateAlbum(){
  //   console.log('Updating albums...');
  // }
}//End of Class

export interface Album {
  title: string,
  artist: string,
  songs: string[],
  favorite: string,
  year: string,
  genre: string,
  units: number,
  cover?: string
}




