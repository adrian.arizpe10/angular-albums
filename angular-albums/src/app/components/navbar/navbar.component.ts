import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  websiteName: string;
  icon: string;

  constructor() { }

  ngOnInit(): void {

    this.websiteName = 'Album\'s Website';
    this.icon = 'assets/images/download.png'
  }//End of ngOnInit

}//End of Class
