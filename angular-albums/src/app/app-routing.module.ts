import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AlbumsComponent } from "./components/albums/albums.component";
import {HomeComponent} from "./components/home/home.component";
import {SpotifyplayerComponent} from "./components/spotifyplayer/spotifyplayer.component";

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'albums',
    component: AlbumsComponent
  },
  {
    path: 'spotifyplayer',
    component: SpotifyplayerComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
