import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ParticlesModule} from 'angular-particle';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AlbumsComponent } from './components/albums/albums.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';
import {FormsModule} from "@angular/forms";
import { HomeComponent } from './components/home/home.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SpotifyplayerComponent } from './components/spotifyplayer/spotifyplayer.component';

@NgModule({
  declarations: [
    AppComponent,
    AlbumsComponent,
    NavbarComponent,
    FooterComponent,
    HomeComponent,
    SpotifyplayerComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ParticlesModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
